public class Producer extends Thread {
	SynchQueue<Integer> q;
	int curr;
	int maxSize;
	
	public Producer(SynchQueue<Integer> q, int maxSize) {
		this.q = q;
		this.curr = 1;
		this.maxSize = maxSize;
	}
	
	public void run() {
		for(;;) {			
			synchronized (q) {
				while(q.GetSize() == maxSize) {
					try {
						q.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				if(q.GetSize() <= maxSize * 1/2) {
					Integer i = new Integer(curr);
					q.Add(i);
					curr++;
					q.notify();
				}
			}
		}
	}
}
