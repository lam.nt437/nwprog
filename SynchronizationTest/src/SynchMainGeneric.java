
public class SynchMainGeneric {
	public static void main(String[] args) {
		SynchQueue<Integer> q = new SynchQueue<Integer>();
		Producer p = new Producer(q, 4);
		Consumer c = new Consumer(q, 4);
		
		p.start();
		c.start();
	}
}
