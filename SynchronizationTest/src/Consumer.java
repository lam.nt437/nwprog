
public class Consumer extends Thread {
	public SynchQueue<Integer> q;
	public int maxSize;
	
	public Consumer(SynchQueue<Integer> q, int maxSize) {
		this.q = q;
		this.maxSize = maxSize;
	}
	
	public void run() {
		for(;;) {			
			synchronized (q) {
				while(q.IsEmpty()) {
					try {
						q.wait();
					} catch(InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				Integer i = q.Remove();
				if(i != null) {
					System.out.print(i + " ");
				}
				q.notify();
			}
		}
	}
}
