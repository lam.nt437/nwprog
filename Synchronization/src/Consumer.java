
public class Consumer extends Thread {
	public SynchQueue<Integer> q;
	
	public Consumer(SynchQueue<Integer> q) {
		this.q = q;
	}
	
	public void run() {
		for(;;) {
			Integer i = q.Remove();
			if(i != null) {
				System.out.print(i + " ");
			}
		}
	}
}
