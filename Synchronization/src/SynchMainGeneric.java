
import java.util.LinkedList;

public class SynchMainGeneric {
	public static void main(String[] args) {
		SynchQueue<Integer> q = new SynchQueue<Integer>();
		Producer p = new Producer(q);
		Consumer c = new Consumer(q);
		
		p.start();
		c.start();
	}
}
