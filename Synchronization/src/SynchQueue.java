import java.util.LinkedList;


public class SynchQueue<DataType> {
	public LinkedList<DataType> l;
	
	public SynchQueue() {
		this.l = new LinkedList<DataType>();
	}
	
	public synchronized void Add(DataType elm) {
		l.addLast(elm);
	}
	
	public synchronized DataType Remove() {
		if(l.size() > 0) {
			DataType elm = l.removeFirst();
			return elm;
		} else {
			return null;
		}
	}
}
