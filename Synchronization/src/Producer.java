public class Producer extends Thread {
	SynchQueue<Integer> q;
	int curr;
	
	public Producer(SynchQueue<Integer> q) {
		this.q = q;
		this.curr = 1;
	}
	
	public void run() {
		for(;;) {
			Integer i = new Integer(curr);
			q.Add(i);
			curr++;
		}
	}
}
